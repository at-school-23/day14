package ru.devray.day14;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.files.DownloadActions.click;

public class OrangeHRMTest {

    SelenideElement userNameField = $x("//input[@name='username']");
    SelenideElement passwordField = $x("//input[@name='password']");
    SelenideElement loginButton = $x("//button[text()=' Login ']");
    SelenideElement menuSectionRecruitment = $x("//span[text()='Recruitment']");
    SelenideElement firstCandidateTableEntryCheckbox = $x("//div[@class='oxd-table-body']//div[@role='row']//input[1]/following-sibling::span");
    SelenideElement candidateFilterButton = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/i");

    @Test
    public void test() {
        //Selenide
        open("https://opensource-demo.orangehrmlive.com/");
        $x("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input").sendKeys("Admin");
        $x("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input").sendKeys("admin123");

        /*   */

//        SelenideElement nonExistingELement = $x("23423432423423");//1. при взаимодействии 2. при попытке проверки элемента
//        nonExistingELement.click();
//        nonExistingELement.shouldBe(Condition.visible);

        sleep(2000);
    }

    @Test
    public void test2() {
        //Selenide
        open("https://opensource-demo.orangehrmlive.com/");

        userNameField.sendKeys("Admin");
        passwordField.sendKeys("admin123");
        loginButton.click();
        menuSectionRecruitment.click();
        firstCandidateTableEntryCheckbox.click();
        candidateFilterButton.click();
        firstCandidateTableEntryCheckbox.click();

        sleep(3000);
    }
}
