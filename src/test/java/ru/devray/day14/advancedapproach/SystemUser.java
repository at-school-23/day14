package ru.devray.day14.advancedapproach;

public enum SystemUser {
    ADMIN("Admin", "admin123"),
    HR_MANAGER("","");

    public String username;
    public String password;

    SystemUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
