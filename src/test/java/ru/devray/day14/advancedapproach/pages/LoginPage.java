package ru.devray.day14.advancedapproach.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import ru.devray.day14.advancedapproach.SystemUser;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    //1. элементы страницы  SelenideElement

    SelenideElement userNameField = $x("//input[@name='username']");
    SelenideElement passwordField = $x("//input[@name='password']");
    SelenideElement loginButton = $x("//button[text()=' Login ']");

    //2. методы для работы с элементами

    public void open() {
        Selenide.open("https://opensource-demo.orangehrmlive.com");
    }


    public void setUserName(String username) {
        userNameField.sendKeys(username);
    }

    public void setPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void loginUser(SystemUser user) {
        setUserName(user.username);
        setPassword(user.password);
        clickLoginButton();
    }

    public void clickLoginButton() {
        loginButton.click();
    }

}
