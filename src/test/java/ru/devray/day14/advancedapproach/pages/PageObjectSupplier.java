package ru.devray.day14.advancedapproach.pages;

import ru.devray.day14.newapproanch.pages.DashboardPage;

public interface PageObjectSupplier {
    default LoginPage loginPage() {
        return new LoginPage();
    }
    default DashboardPage dashboardPage() {
        return new DashboardPage();
    }
    default RecruitmentPage recruitmentPage() {
        return new RecruitmentPage();
    }
}
