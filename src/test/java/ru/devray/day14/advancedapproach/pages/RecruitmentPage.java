package ru.devray.day14.advancedapproach.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class RecruitmentPage {

    String candidateTableEntryCheckboxXpathTemplate = "//div[@class='oxd-table-body']//div[@role='row']//input[%d]/following-sibling::span";

    SelenideElement firstCandidateTableEntryCheckbox = $x("//div[@class='oxd-table-body']//div[@role='row']//input[1]/following-sibling::span");
    SelenideElement candidateFilterButton = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/i");
    SelenideElement candidateFilterAscendingOption = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/div/ul/li[1]/span");

    ElementsCollection menuItems = $$x("//ul[@class='oxd-main-menu']/li");


    public void selectCandidateCheckbox(int candidateIndex) {
        $x(candidateTableEntryCheckboxXpathTemplate.formatted(candidateIndex)).click();
    }

    public void clickCandidateFilterButton() {
        candidateFilterButton.click();
    }

    public void selectFilterByAscensionButton() {
        candidateFilterAscendingOption.click();
    }

    public void checkAllMenuItemsVisible() {
//        menuItems.shouldBe(CollectionCondition);
//        for (SelenideElement menuItem)
    }

}
