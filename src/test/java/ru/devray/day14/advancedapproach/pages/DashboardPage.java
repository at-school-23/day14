package ru.devray.day14.advancedapproach.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {

    SelenideElement menuSectionRecruitment = $x("//span[text()='Recruitment']");
    SelenideElement menuSectionMaintenance = $x("//span[text()='Maintenance']");

    String menuItemXpathTemplate = "//span[text()='%s']";

    public void clickRecruitmentMenuItem() {
        menuSectionRecruitment.click();
    }

    public void clickMenuItem(String menuItemName) {
        $x(String.format(menuItemXpathTemplate, menuItemName)).click();
    }

}
