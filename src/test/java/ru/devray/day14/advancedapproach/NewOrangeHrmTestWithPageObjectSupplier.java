package ru.devray.day14.advancedapproach;

import org.testng.annotations.Test;
import ru.devray.day14.advancedapproach.pages.DashboardPage;
import ru.devray.day14.advancedapproach.pages.LoginPage;
import ru.devray.day14.advancedapproach.pages.PageObjectSupplier;
import ru.devray.day14.advancedapproach.pages.RecruitmentPage;

import static ru.devray.day14.advancedapproach.SystemUser.ADMIN;


public class NewOrangeHrmTestWithPageObjectSupplier implements PageObjectSupplier {

    @Test
    public void test() {
        loginPage().open();
        loginPage().loginUser(ADMIN);

        dashboardPage().clickMenuItem("Recruitment");

        recruitmentPage().selectCandidateCheckbox(1);
        recruitmentPage().clickCandidateFilterButton();
        recruitmentPage().selectFilterByAscensionButton();

    }
}
