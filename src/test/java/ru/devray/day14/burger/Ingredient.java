package ru.devray.day14.burger;

public enum Ingredient {
    //огурцы, помидоры, котлета, сыр, кетчуп, майонез, булочка, салат.

    CUCUMBER,
    TOMATO,
    BEEF,
    CHEESE,
    KETCHUP,
    MAYONNAISE,
    BUN,
    SALAD;

}
