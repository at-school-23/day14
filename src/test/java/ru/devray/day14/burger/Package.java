package ru.devray.day14.burger;

import java.util.List;

public class Package {

    List<Ingredient> ingredients;

    public Package(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Package{" +
                "ingredients=" + ingredients +
                '}';
    }
}
