package ru.devray.day14.burger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class BurgerPackageSupplier {

    private static Random random = new Random();

    public Package generateRandomPackage(int ingredientsCount) {
        Ingredient[] ingredientValues = Ingredient.values();
        List<Ingredient> ingredientsList = new ArrayList<>();

        for (int i = 0; i < ingredientsCount; i++) {
            int randomIngredientIndex = random.nextInt(ingredientValues.length);
            Ingredient ingredient = ingredientValues[randomIngredientIndex];
            ingredientsList.add(ingredient);
        }

        return new Package(ingredientsList);
    }

    public Package generateRandomPackage() {
        int ingredientsCount = random.nextInt(3, 12);
        return generateRandomPackage(ingredientsCount);
    }

}
