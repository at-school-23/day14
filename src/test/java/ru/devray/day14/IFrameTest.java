package ru.devray.day14;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

public class IFrameTest {
    @Test
    public void test() {
        open("https://the-internet.herokuapp.com/iframe");
        switchTo().frame("mce_0_ifr");

        SelenideElement textEditorField = $x("//body/p");
        textEditorField.sendKeys("Hello world, today I forgot how to chech Collection Condition visible");

        switchTo().parentFrame();

//        WebDriverRunner.getWebDriver().manage().getCookieNamed()
//        Selenide.executeJavaScript("localStorage.getItem()")


        sleep(2000);
    }
}
