package ru.devray.day14.newapproanch;

import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.Test;
import ru.devray.day14.newapproanch.pages.DashboardPage;
import ru.devray.day14.newapproanch.pages.LoginPage;
import ru.devray.day14.newapproanch.pages.RecruitmentPage;

import static ru.devray.day14.newapproanch.SystemUser.ADMIN;

public class NewOrangeHrmTestWithPageObjects {

    //        loginPage.setUserName("Admin");
//        loginPage.setPassword("admin123");
//        loginPage.setUserName(ADMIN.username);
//        loginPage.setPassword(ADMIN.password);
//        loginPage.clickLoginButton();

    LoginPage loginPage = new LoginPage();
    DashboardPage dashboardPage = new DashboardPage();
    RecruitmentPage recruitmentPage = new RecruitmentPage();

    @Test
    public void test() {
        loginPage.open();
        loginPage.loginUser(ADMIN);

        dashboardPage.clickMenuItem("Recruitment");

        recruitmentPage.selectCandidateCheckbox(1);
        recruitmentPage.clickCandidateFilterButton();
        recruitmentPage.selectFilterByAscensionButton();
    }
}
