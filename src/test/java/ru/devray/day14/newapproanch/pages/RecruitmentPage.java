package ru.devray.day14.newapproanch.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class RecruitmentPage {

    String candidateTableEntryCheckboxXpathTemplate = "//div[@class='oxd-table-body']//div[@role='row']//input[%d]/following-sibling::span";

    SelenideElement firstCandidateTableEntryCheckbox = $x("//div[@class='oxd-table-body']//div[@role='row']//input[1]/following-sibling::span");
    SelenideElement candidateFilterButton = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/i");
    SelenideElement candidateFilterAscendingOption = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/div/ul/li[1]/span");

    public void selectCandidateCheckbox(int candidateIndex) {
        $x(candidateTableEntryCheckboxXpathTemplate.formatted(candidateIndex)).click();
    }

    public void clickCandidateFilterButton() {
        candidateFilterButton.click();
    }

    public void selectFilterByAscensionButton() {
        candidateFilterAscendingOption.click();
    }

}
